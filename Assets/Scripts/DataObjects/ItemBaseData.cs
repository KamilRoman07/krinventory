﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemBaseData", menuName = "Data/Items/ItemBaseData", order = 1)]
public class ItemBaseData : ScriptableObject
{
    public int amount;
    public string itemNameKey;
    public string itemDescriptionKey;
    public string dateAquired;
    public bool stackable = false;
    public bool unique = false;
    public GameObject itemPrefab;
    public Sprite sprite;
}
