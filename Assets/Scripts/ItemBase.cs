﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    ItemBaseData Data();
    ItemBaseData OriginalData();
    void Interact(int actionIndex);
    void StopInterract(int actionIndex);
}

public class ItemBase : MonoBehaviour, IInteractable
{
    [SerializeField] ItemBaseData data;
    ItemBaseData originalData;
    int interactActionIndex = -1;

    public virtual void Awake()
    {
        SetData();
    }

    protected void SetData()
    {
        if (data != null && !data.name.Contains("Clone"))
        {
            originalData = data;
            data = Instantiate(originalData);
        }
    }

    public virtual void SetData(ItemBaseData dataToSet)
    {
        data = dataToSet;
    }

    public virtual ItemBaseData Data()
    {
        return data;
    }

    public virtual T Data<T>() where T : ItemBaseData
    {
        return data as T;
    }

    public virtual ItemBaseData OriginalData()
    {
        return originalData;
    }

    public virtual void Interact(int actionIndex)
    {
        interactActionIndex = actionIndex;
        switch (interactActionIndex)
        {
            case 0:
                InventoryManager.Instance.AddItemToInventory(Data());
                break;
            default:
                break;
        }
    }

    public virtual void StopInterract(int actionIndex)
    {
        interactActionIndex = -1;
    }
}
