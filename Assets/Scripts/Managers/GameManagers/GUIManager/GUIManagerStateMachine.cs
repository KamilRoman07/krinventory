﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManagerStateMachine : MonoBehaviour
{
    [SerializeField]
    [RequireInterface(typeof(IGUIState))]
    private List<UnityEngine.Object> guiStates;

    private GUIStateType currentGUIStateType = GUIStateType.None;
    private GUIStateType previousGUIStateType = GUIStateType.None;
    private IGUIState currentGUIState = null;
    private static GUIManagerStateMachine instance = null;

    public static Action<GUIStateType> OnGuiStateSwitch;

    public static GUIManagerStateMachine Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GUIManagerStateMachine>();
            }

            return instance;
        }
    }

    private Action OnExitStateEnd;

    private void OnEnable()
    {
        OnExitStateEnd += EnterStateAfterExitOfOldState;
    }

    private void OnDisable()
    {
        OnExitStateEnd -= EnterStateAfterExitOfOldState;
    }

    public GUIStateType CurrentGUIStateType
    {
        get => currentGUIStateType;
    }

    public GUIStateType PreviousGUIStateType
    {
        get => previousGUIStateType;
    }

    public void SwitchStateType(GUIStateType guiStateType)
    {
        if (currentGUIStateType != guiStateType)
        {
            previousGUIStateType = currentGUIStateType;
            currentGUIStateType = guiStateType;
            if (currentGUIState == null)
            {
                currentGUIState = GetStateByEnum(GUIStateType.None);
            }
            currentGUIState.ExitState(OnExitStateEnd);
        }
    }

    private IGUIState GetStateByEnum(GUIStateType guiStateType)
    {
        foreach (IGUIState guiState in guiStates)
        {
            if (guiState.IsValid(guiStateType))
            {
                return guiState;
            }
        }
        Debug.LogError("Not All States Are Attached");
        return null;
    }

    private void EnterStateAfterExitOfOldState()
    {
        currentGUIState = GetStateByEnum(currentGUIStateType);
        Debug.Log("Enter GUI State: " + currentGUIStateType);
		if (currentGUIState != null)
			currentGUIState.EnterState();
		else
			Debug.LogError("currentGUIState is null");

		OnGuiStateSwitch?.Invoke(currentGUIStateType);
    }
}
