﻿public enum GUIStateType
{
    None = 1 << 0,
    Exploration = 1 << 1,
    Inventory = 1 << 2,
}
