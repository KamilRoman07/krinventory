﻿using System;

public interface IGUIState 
{
    bool IsValid(GUIStateType guiState);

    void EnterState();

    void ExitState(Action actionInvokedOnExitEnd);
}
