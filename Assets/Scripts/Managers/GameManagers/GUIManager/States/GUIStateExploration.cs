﻿using System;
using UnityEngine;

public class GUIStateExploration : MonoBehaviour, IGUIState
{
    private GUIStateType guiStateType = GUIStateType.Exploration;
    public void EnterState()
    {
    }

    public void ExitState(Action actionInvokedOnExitEnd)
    {
        actionInvokedOnExitEnd?.Invoke();
    }

    public bool IsValid(GUIStateType guiState)
    {
        return guiState == guiStateType;
    }
}
