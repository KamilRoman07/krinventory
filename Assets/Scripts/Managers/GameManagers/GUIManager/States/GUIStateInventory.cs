﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIStateInventory : MonoBehaviour, IGUIState
{
    InventoryManager inventoryManager;

    private GUIStateType guiStateType = GUIStateType.Inventory;
    public void EnterState()
    {
        if (!GameManagerStateMachine.Instance.CurrentGameModeType.Equals(GameModeType.GUI))
            GameManagerStateMachine.Instance.SwitchStateType(GameModeType.GUI);
        if (!inventoryManager)
            inventoryManager = Utility.FindObjectOnScenes<InventoryManager>(true);
        inventoryManager.OpenInventory();
    }

    public void ExitState(Action actionInvokedOnExitEnd)
    {
        if (inventoryManager)
            inventoryManager.CloseInventory();
        actionInvokedOnExitEnd?.Invoke();
    }

    public bool IsValid(GUIStateType guiState)
    {
        return guiState == guiStateType;
    }
}
