﻿using System;
using UnityEngine;

public class GUIStateNone : MonoBehaviour, IGUIState
{
    private GUIStateType guiStateType = GUIStateType.None;
    public void EnterState()
    {

    }

    public void ExitState(Action actionInvokedOnExitEnd)
    {
        actionInvokedOnExitEnd?.Invoke();
    }

    public bool IsValid(GUIStateType guiState)
    {
        return guiState == guiStateType;
    }
}
