﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] PlayerData playerData;
    public PlayerData CurrentPlayerData => playerData;

    private void Start()
    {
        GameManagerStateMachine.Instance.SwitchStateType(GameModeType.Exploration);
    }
}
