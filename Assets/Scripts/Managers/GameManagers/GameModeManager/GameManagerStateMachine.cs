﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerStateMachine : MonoBehaviour
{
    [SerializeField]
    [RequireInterface(typeof(IGameMode))]
    private List<UnityEngine.Object> gameModes;

    private GameModeType currentGameModeType = GameModeType.None;
    private GameModeType nextGameModeType = GameModeType.None;
    private GameModeType previousGameModeType = GameModeType.None;
    private GameModeType futureGameModeType = GameModeType.None;
    private IGameMode currentGameMode = null;
    private static GameManagerStateMachine instance = null;
    private GameObject currentGameModeObjectInFocus;
    private GameObject nextGameModeObjectInFocus;


    public static Action<GameModeType, GameModeType, GameObject> OnGameModeSwitch; // from to
    private Action<GameObject> OnExitStateEnd;
    private Action OnStartStateEnd;
	private string switchStateStackTrace;


	public static GameManagerStateMachine Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManagerStateMachine>();
            }

            return instance;
        }
    }

    public static bool IsReady
    {
        get
        {
            return instance;
        }
    }

    public GameObject CurrentGameModeObjectInFocus
    {
        get => currentGameModeObjectInFocus;
    }

    public GameObject NextGameModeObjectInFocus
    {
        get => nextGameModeObjectInFocus;
    }

    public GameModeType CurrentGameModeType
    {
        get => currentGameModeType;
    }

    public GameModeType PreviousGameModeType
    {
        get => previousGameModeType;
    }

    public GameModeType NextGameModeType
    {
        get => nextGameModeType;
    }

    private void OnEnable()
    {
        OnExitStateEnd += EnterStateAfterExitOfOldState;
        OnStartStateEnd += SwitchStateAfterStartEnd;
    }

    private void OnDisable()
    {
        OnExitStateEnd -= EnterStateAfterExitOfOldState;
        OnStartStateEnd -= SwitchStateAfterStartEnd;
    }

    public void SwitchStateType(GameModeType gameModeType, GameObject objectInFocus = null)
    {
		switchStateStackTrace = Environment.StackTrace;

        if (currentGameModeType != gameModeType)
        {
            nextGameModeType = gameModeType;
            nextGameModeObjectInFocus = objectInFocus;

            if (currentGameMode == null)
            {
                currentGameMode = GetStateByEnum(GameModeType.None);
            }

            previousGameModeType = currentGameModeType;
            currentGameModeType = GameModeType.None;
            currentGameModeObjectInFocus = null;
			try
            {
                OnGameModeSwitch?.Invoke(previousGameModeType, currentGameModeType, objectInFocus);
			}

			catch (Exception e)
            {
                Debug.LogError(e.Message + (!string.IsNullOrEmpty(switchStateStackTrace) ? " SwitchStateStack: " + switchStateStackTrace : ""));
            }

            try
            {
			currentGameMode.ExitGameMode(
                    () =>
                    {
                        try
                        {
                            OnExitStateEnd(objectInFocus);
                        }
                        catch (Exception e)
                        {
                            Debug.LogError("In OnExitStateEnd when exiting from: " + previousGameModeType + " " + e.Message + (!string.IsNullOrEmpty(switchStateStackTrace) ? " SwitchStateStack: " + switchStateStackTrace : ""));

						}
                    }
                    );
			}

			catch (Exception e)
            {
                Debug.LogError(e.Message + (!string.IsNullOrEmpty(switchStateStackTrace) ? " SwitchStateStack: " + switchStateStackTrace : ""));

                try
                {
					OnExitStateEnd(objectInFocus);
                }
                catch (Exception e2)
                {
                    Debug.LogError("In OnExitStateEnd when exiting from: " + previousGameModeType + " " + e2.Message + (!string.IsNullOrEmpty(switchStateStackTrace) ? " SwitchStateStack: " + switchStateStackTrace : ""));
                }
            }
		}
	}

	private IGameMode GetStateByEnum(GameModeType gameModeType)
    {
        foreach (IGameMode gameState in gameModes)
        {
            if (gameState.IsValid(gameModeType))
            {
                return gameState;
            }
        }
        Debug.LogError("Not All GameModes Are Attached");
        return null;
    }

    private void EnterStateAfterExitOfOldState(GameObject objectInFocus)
    {
        Debug.Log("Exit GameMode: " + currentGameMode);
        currentGameMode = GetStateByEnum(nextGameModeType);
		try
		{
			if (currentGameMode != null)
                currentGameMode.EnterGameMode(
                        () =>
                        {
                            try
                            {
                                OnStartStateEnd?.Invoke();
                            }
                            catch (Exception e)
                            {
                                Debug.LogError("In OnStartStateEnd when going into: " + currentGameMode + " " + e.Message + (!string.IsNullOrEmpty(switchStateStackTrace) ? " SwitchStateStack: " + switchStateStackTrace : ""));
                            }
                        },
                        objectInFocus
                        );
            else
                Debug.LogError("currentGameMode == null");
		}
        catch (Exception e)
        {
            Debug.LogError(e.Message);

            try
            {
                OnStartStateEnd();
            }
            catch (Exception e2)
            {
                Debug.LogError("In OnStartStateEnd when going into: " + currentGameMode + " " + e2.Message + (!string.IsNullOrEmpty(switchStateStackTrace) ? " SwitchStateStack: " + switchStateStackTrace : ""));
            }
        }
	}

	private void SwitchStateAfterStartEnd()
    {
        currentGameModeType = nextGameModeType;
        currentGameModeObjectInFocus = nextGameModeObjectInFocus;

        Debug.Log("Enter GameMode: " + currentGameModeType);
		try
		{
		OnGameModeSwitch?.Invoke(previousGameModeType, currentGameModeType, currentGameModeObjectInFocus);

		}
		catch (Exception e)
        {
            Debug.LogError(e.Message + (!string.IsNullOrEmpty(switchStateStackTrace) ? " SwitchStateStack: " + switchStateStackTrace : ""));
        }
	}
}
