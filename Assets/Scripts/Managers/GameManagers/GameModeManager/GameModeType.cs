﻿using System;

[Flags]
public enum GameModeType
{
    None = 1 << 0,
    Exploration = 1 << 1,
    GUI = 1 << 2,
}

