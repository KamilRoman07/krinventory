﻿using System;
using UnityEngine;

public interface IGameMode
{
    bool IsValid(GameModeType gameMode);

    void EnterGameMode(Action actionInvokedOnEnterEnd, GameObject objectInFocus);

    void ExitGameMode(Action actionInvokedOnExitEnd);
}
