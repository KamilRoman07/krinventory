﻿using System;
using UnityEngine;


public class GameModeExploration : MonoBehaviour, IGameMode
{
    private GameModeType gameMode = GameModeType.Exploration;

    public void EnterGameMode(Action actionInvokedOnEnterEnd, GameObject objectInFocus)
    {
        GUIManagerStateMachine.Instance.SwitchStateType(GUIStateType.Exploration);
        actionInvokedOnEnterEnd?.Invoke();
	}

    public void ExitGameMode(Action actionInvokedOnExitEnd)
    {
        actionInvokedOnExitEnd?.Invoke();
    }

    public bool IsValid(GameModeType gameMode)
    {
        return this.gameMode == gameMode;
    }
}
