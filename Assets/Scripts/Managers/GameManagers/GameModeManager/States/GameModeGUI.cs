﻿using System;
using System.Collections;
using UnityEngine;

public class GameModeGUI : MonoBehaviour, IGameMode
{
    private GameModeType gameMode = GameModeType.GUI;

    public void EnterGameMode(Action actionInvokedOnEnterEnd, GameObject objectInFocus)
    {
        actionInvokedOnEnterEnd?.Invoke();
    }

    public void ExitGameMode(Action actionInvokedOnExitEnd)
    {
        actionInvokedOnExitEnd?.Invoke();
    }

    public bool IsValid(GameModeType gameMode)
    {
        return this.gameMode == gameMode;
    }
}
