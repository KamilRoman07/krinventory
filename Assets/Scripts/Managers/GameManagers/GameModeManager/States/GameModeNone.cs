﻿using System;
using UnityEngine;

public class GameModeNone : MonoBehaviour, IGameMode
{
    private GameModeType gameMode = GameModeType.None;
    public void EnterGameMode(Action actionInvokedOnEnterEnd, GameObject objectInFocus)
    {
        actionInvokedOnEnterEnd?.Invoke();
    }

    public void ExitGameMode(Action actionInvokedOnExitEnd)
    {
        actionInvokedOnExitEnd?.Invoke();
    }

    public bool IsValid(GameModeType gameMode)
    {
        return this.gameMode == gameMode;
    }
}
