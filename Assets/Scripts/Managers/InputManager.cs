﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public int GetAxis(KeyCode positive, KeyCode negative)
    {
        if (Input.GetKey(positive))
            return 1;
        if (Input.GetKey(negative))
            return - 1;
        return 0;
    }
}
