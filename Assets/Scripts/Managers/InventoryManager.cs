﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : Singleton<InventoryManager>
{
    [SerializeField] WarningPrompt warningPrompt;
    [SerializeField]List<ItemBaseData> itemsInInventory = new List<ItemBaseData>();
    public List<ItemBaseData> ItemsInInventory => itemsInInventory;

    #region Exploration funcitons
    public void AddItemToInventory(ItemBaseData itemBaseData)
    {
        if (itemBaseData.stackable)
            TryToAddStackable(itemBaseData);
        else
            TryToAddUnstackable(itemBaseData);
    }

    bool CheckForFreeBagSpace()
    {
        if (itemsInInventory.Count + 1 <= GameManager.Instance.CurrentPlayerData.maksItemSlotsInInventory)
            return true;
        else
            foreach (ItemBaseData item in itemsInInventory)
                if (item == null)
                    return true;
        return false;
    }

    private bool CheckUnique(ItemBaseData itemBaseData)
    {
        if (!itemBaseData.unique)
            return true;

        if (itemsInInventory.Contains(itemBaseData))
            return false;

        return true;
    }

    private void TryToAddStackable(ItemBaseData itemBaseData)
    {
        if (itemsInInventory.Contains(itemBaseData))
            itemsInInventory[itemsInInventory.IndexOf(itemBaseData)].amount += itemBaseData.amount;
        else if (CheckForFreeBagSpace())
            itemsInInventory.Add(itemBaseData);
        else
            warningPrompt.FlashMessage("Cannot carry anymore");
    }

    private void TryToAddUnstackable(ItemBaseData itemBaseData)
    {
        if (CheckForFreeBagSpace() && CheckUnique(itemBaseData))
        {
            for(int i = 0; i < itemsInInventory.Count; i++)
            {
                if (itemsInInventory[i] == null)
                {
                    itemsInInventory[i] = itemBaseData;
                    return;
                }
            }
            itemsInInventory.Add(itemBaseData);
        }
        else
            warningPrompt.FlashMessage("Cannot carry anymore");
    }

    public void SwapItems(ItemBaseData item1, ItemBaseData item2)
    {
        int index1 = itemsInInventory.IndexOf(item1);
        int index2 = itemsInInventory.IndexOf(item2);
        if (index1 != index2)
        {
            itemsInInventory[index1] = item2;
            itemsInInventory[index2] = item1;
        }
        RefreshPanels();
        currentlyChosenItem1 = null;      
    }

    public void SwapPositionsWithEmpty(int index)
    {
        int index1 = itemsInInventory.IndexOf(currentlyChosenItem1);
        itemsInInventory[index] = currentlyChosenItem1;
        itemsInInventory[index1] = null;
        RefreshPanels();
        currentlyChosenItem1 = null;
    }

    public void RemoveFromInventory(ItemBaseData itemToRemove)
    {
        if (itemsInInventory.Contains(itemToRemove))
            itemsInInventory[itemsInInventory.IndexOf(itemToRemove)] = null;
        else
            Debug.LogError("Something went wrong during removing an item");
    }
    #endregion

    #region GUI functions
    [Header("-- GUI VARIABLES --")]
    [SerializeField] GameObject inventoryMainPanel;
    [SerializeField] GameObject folderButtonPrefab;
    [SerializeField] Transform folderButtonsContent;
    [SerializeField] GameObject folderPrefab;
    [SerializeField] Transform folderPanelsContent;

    List<InventoryFolder> folderButtons = new List<InventoryFolder>();
    List<SingleInventoryPanel> inventoryFolders = new List<SingleInventoryPanel>();

    ItemBaseData currentlyChosenItem1;

    internal void OpenInventory()
    {
        inventoryMainPanel.SetActive(true);
        GenerateTabButtonsAndInventoryPanels();
    }

    private void GenerateTabButtonsAndInventoryPanels()
    {
        CleanTabButtons();
        CleanPanels();

        int panelAmountToSpawn = Mathf.CeilToInt((float)GameManager.Instance.CurrentPlayerData.maksItemSlotsInInventory / 28f);

        for(int i = 0; i < panelAmountToSpawn; i++)
        {
            GameObject folderButtonGameObject = Instantiate(folderButtonPrefab);
            folderButtonGameObject.transform.SetParent(folderButtonsContent, true);
            folderButtonGameObject.transform.localScale = Vector3.one;
            GameObject folderGameObject = Instantiate(folderPrefab);
            folderGameObject.transform.SetParent(folderPanelsContent, true);
            folderGameObject.transform.localScale = Vector3.one;
            folderGameObject.transform.localPosition = Vector3.zero;

            InventoryFolder folderButton = folderButtonGameObject.GetComponent<InventoryFolder>();
            SingleInventoryPanel folder = folderGameObject.GetComponent<SingleInventoryPanel>();

            folderButton.SetUp(i, folder, () => {
                DisableAllFolderButtons();
                DisableAllFolderPanels();
                });
            folder.SetUp(i, itemsInInventory, i == panelAmountToSpawn - 1);

            folderButtons.Add(folderButton);
            inventoryFolders.Add(folder);

        }

        if (folderButtons.Count > 0)
            folderButtons[0].OnClick();
    }

    private void CleanTabButtons()
    {
        foreach (Transform t in folderButtonsContent)
            Destroy(t.gameObject);
        folderButtons.Clear() ;
    }

    private void CleanPanels()
    {
        foreach (Transform t in folderPanelsContent)
            Destroy(t.gameObject);
        inventoryFolders.Clear() ;
    }

    private void DisableAllFolderButtons()
    {
        foreach (InventoryFolder button in folderButtons)
            button.Deselect();
    }

    private void DisableAllFolderPanels()
    {
        foreach (SingleInventoryPanel folder in inventoryFolders)
        {
            folder.isActive = false;
            folder.gameObject.SetActive(false);
        }
    }

    internal void CloseInventory()
    {
        inventoryMainPanel.SetActive(false);
    }

    public void DropCurrentlyChosenItem()
    {
        if (currentlyChosenItem1)
        {
            //TODO - drop item on the ground
            DestroyCurrentlyChosenItem();
        }
    }

    public void DestroyCurrentlyChosenItem()
    {
        if (currentlyChosenItem1)
        {
            RemoveFromInventory(currentlyChosenItem1);
            currentlyChosenItem1 = null;
            RefreshPanels();
        }
    }

    void RefreshPanels()
    {
        foreach (SingleInventoryPanel folder in inventoryFolders)
            folder.Refresh();
    }

    public void ItemButtonOnClickAction(ItemBaseData itemData)
    {
        if (currentlyChosenItem1 == null)
            currentlyChosenItem1 = itemData;
        else
            SwapItems(currentlyChosenItem1, itemData);
    }

    internal void ClickedEmptySpace(int globalIndex)
    {
        if (itemsInInventory.Count < globalIndex + 1) {
            while (itemsInInventory.Count <= globalIndex)
                itemsInInventory.Add(null);
        }
        if (currentlyChosenItem1)
            SwapPositionsWithEmpty(globalIndex);
    }
    #endregion
}
