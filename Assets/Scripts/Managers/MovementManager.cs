﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : Singleton<MovementManager>
{
    [Header("--MOVEMENT KEYS--")]
    [SerializeField] KeyCode forward = KeyCode.W;
    [SerializeField] KeyCode back = KeyCode.S;
    [SerializeField] KeyCode left = KeyCode.A;
    [SerializeField] KeyCode right = KeyCode.D;
    [SerializeField] KeyCode inventory = KeyCode.I;
    [SerializeField] KeyCode interact = KeyCode.Mouse0;

    [Header("--VARIABLES--")]
    [SerializeField] CharacterController playerController;
    [SerializeField] Camera mainPlayerCamera;
    [SerializeField] CinemachineVirtualCamera mainVirtualCamera;
    [SerializeField] float speed = 10;

    [Header("--CAMERA VARIABLES--")]
    [SerializeField] public float cameraSensitivityFactor = 1.0f;
    [SerializeField] float lookDownThreshold = 85f;
    [SerializeField] float lookUpThreshold = 300f;

    RaycastManager raycastManager;
    InputManager inputManager;

    private void Awake()
    {
        mainVirtualCamera.Priority = Globals.activeCinemachineCameraPriority;
        raycastManager = RaycastManager.Instance;
        inputManager = InputManager.Instance;
    }

    private void Update()
    {
        HandleMovement();
        HandleCameraRotation();
        HandleButtons();
    }

    #region Movement
    private void HandleMovement()
    {
        switch (GameManagerStateMachine.Instance.CurrentGameModeType)
        {
            case GameModeType.Exploration:
                HandleFreeMovement();
                break;
        }
    }

    private void HandleFreeMovement()
    {
        Vector3 movement = new Vector3(inputManager.GetAxis(right,left), 0.0f, inputManager.GetAxis(forward, back));
        if (movement.magnitude > 0.0f)
            playerController.Move(transform.rotation * movement * Time.deltaTime * speed);
    }
    #endregion

    #region Camera rotation
    private void HandleCameraRotation()
    {
        switch (GameManagerStateMachine.Instance.CurrentGameModeType)
        {
            case GameModeType.Exploration:
                HandleFreeMovementCameraRotation();
                break;
        }
    }

    private void HandleFreeMovementCameraRotation()
    {
        if (Input.GetAxis("Mouse X") != 0)
            transform.Rotate(0, cameraSensitivityFactor * Input.GetAxis("Mouse X"), 0);

        if (Input.GetAxis("Mouse Y") != 0)
        {
            float cameraRotationX = mainVirtualCamera.transform.localEulerAngles.x;
            cameraRotationX += -cameraSensitivityFactor * Input.GetAxis("Mouse Y");

            //-180 to differ minus/plus below/above horizon
            float targetRotation = cameraRotationX - 180f;

            bool isMouseMovingDown = Input.GetAxis("Mouse Y") < 0;
            bool isMouseMovingUp = Input.GetAxis("Mouse Y") > 0;
            bool isTargetRotationAboveHorizon = targetRotation > 0;
            bool isTargetRotationBelowHorizon = targetRotation < 0;
            bool isTargetRotationWithinDownThreshold = targetRotation < (lookDownThreshold - 180f);
            bool isTargetRotationWithinUpThreshold = targetRotation > (lookUpThreshold - 180f);

            if (isMouseMovingDown)
            {
                bool isTargetRotationAcceptable = isTargetRotationBelowHorizon && isTargetRotationWithinDownThreshold;

                if (isTargetRotationAboveHorizon || isTargetRotationAcceptable)
                    mainVirtualCamera.transform.Rotate(-cameraSensitivityFactor * Input.GetAxis("Mouse Y"), 0, 0);
                else if (!isTargetRotationAcceptable)
                    mainVirtualCamera.transform.localEulerAngles = new Vector3(lookDownThreshold, mainVirtualCamera.transform.localEulerAngles.y, mainVirtualCamera.transform.localEulerAngles.z);
            }
            if (isMouseMovingUp)
            {
                bool isTargetRotationAcceptable = isTargetRotationAboveHorizon && isTargetRotationWithinUpThreshold;

                if (isTargetRotationBelowHorizon || isTargetRotationAcceptable)
                    mainVirtualCamera.transform.Rotate(-cameraSensitivityFactor * Input.GetAxis("Mouse Y"), 0, 0);
                else if (!isTargetRotationAcceptable)
                    mainVirtualCamera.transform.localEulerAngles = new Vector3(lookUpThreshold, mainVirtualCamera.transform.localEulerAngles.y, mainVirtualCamera.transform.localEulerAngles.z);
            }
        }
    }
    #endregion

    #region Buttons
    void HandleButtons()
    {
        HandleInteract();
        HandleInventory();
    }

    void HandleInteract()
    {
        if (Input.GetKeyDown(interact) && raycastManager.GetRaycastHitObject() != null && raycastManager.GetRaycastHitObject().gameObject.TryGetComponent<ItemBase>(out ItemBase item))
            item.Interact(0);
    }

    void HandleInventory()
    {
        if (Input.GetKeyDown(inventory))
        {
            if (!GameManagerStateMachine.Instance.CurrentGameModeType.Equals(GameModeType.GUI) && !GUIManagerStateMachine.Instance.CurrentGUIStateType.Equals(GUIStateType.Inventory))
                GUIManagerStateMachine.Instance.SwitchStateType(GUIStateType.Inventory);
            else
                GameManagerStateMachine.Instance.SwitchStateType(GameModeType.Exploration);
        }
    }
    #endregion

    internal Transform GetPlayerCameraTransform()
    {
        return mainPlayerCamera.transform;
    }
}
