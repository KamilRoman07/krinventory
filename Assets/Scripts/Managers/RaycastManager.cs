﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class RaycastManager : Singleton<RaycastManager>
{

    [SerializeField] private float raycastRange = 2f;
    bool castRay = true;
    int ignoreLayers = 0;

    GameManagerStateMachine stateMachineInstance;
    Transform mainCameraTransform;
    private Collider previous;
    RaycastHit hit;

    public bool IsCasting => castRay;
    public RaycastHit GetRaycastHit() => hit;
    public Vector3 GetRaycastHitPoint() => hit.point;
    public Collider GetRaycastHitObject() => hit.collider;
    public ItemBase lastSeenItem { get; private set; }

    public event Action<Collider> OnRayEnter;
    public event Action<Collider> OnRayStay;
    public event Action<Collider> OnRayExit;

    void Start()
    {
        stateMachineInstance = GameManagerStateMachine.Instance;
        mainCameraTransform = MovementManager.Instance.GetPlayerCameraTransform();
        ignoreLayers = LayerMask.GetMask(Globals.raycastIgnorableLayers);
        this.OnRayEnter += Ray_OnEnter;
        this.OnRayExit += Ray_OnExit;
    }

    // Update is called once per frame
    void Update()
    {
        if (castRay && mainCameraTransform != null)
        {
            if (stateMachineInstance.CurrentGameModeType.Equals(GameModeType.Exploration))
            {
                CastRay(mainCameraTransform.position, mainCameraTransform.forward);
            }
            else if (Time.timeScale != 0)
            {
                var mouseRay = mainCameraTransform.gameObject.GetComponent<Camera>().ScreenPointToRay(new Vector2(0.5f,0.5f));
                CastRay(mouseRay.origin, mouseRay.direction);
            }
        }
    }

    private Collider CastRay(Vector3 originPoint, Vector3 direction)
    {
        Utility.ClosestPriorityHit(new Ray(originPoint, direction), raycastRange, out hit, ~ignoreLayers, new string[] { "Character" }, false, new string[] {  });

        if (hit.collider)
        {
            RaycastHit testHit = default;
            Utility.ClosestPriorityHit(new Ray(originPoint, direction), raycastRange, out testHit, ~ignoreLayers, new string[] { "Character" }, false, new string[] {  });


            if (testHit.collider)
                hit = testHit;
        }

        ProcessCollision(hit.collider);
        ItemBase item = null;
        if (hit.collider && hit.collider.TryGetComponent(out item) && item != lastSeenItem)
            lastSeenItem = item;

        return hit.collider;
    }

    private void ProcessCollision(Collider current)
    {
        // No collision this frame.
        if (current == null)
        {
            // But there was an object hit last frame.
            if (previous != null)
                DoEvent(OnRayExit, previous);
        }

        // The object is the same as last frame.
        else if (previous == current)
        {
            DoEvent(OnRayStay, current);
        }

        // The object is different than last frame.
        else if (previous != null)
        {
            DoEvent(OnRayExit, previous);
            DoEvent(OnRayEnter, current);
        }

        // There was no object hit last frame.
        else
        {
            DoEvent(OnRayEnter, current);
        }

        // Remember this object for comparing with next frame.
        previous = current;
    }

    private void DoEvent(Action<Collider> action, Collider collider)
    {
        action?.Invoke(collider);
    }

    private void Ray_OnEnter(Collider collider)
    {
    }

    private void Ray_OnExit(Collider collider)
    {
    }

    public void StopCasting()
    {
        castRay = false;
    }

    public void CastRay()
    {
        castRay = true;
    }
}
