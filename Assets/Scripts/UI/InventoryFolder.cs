﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryFolder : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI folderText;
    [SerializeField] Button myButton;

    SingleInventoryPanel myInventoryPanel;
    Action onClickAction;

    public void SetUp(int i, SingleInventoryPanel folder, Action onClick)
    {
        folderText.SetText("Folder " + (i+1));
        myInventoryPanel = folder;
        onClickAction = onClick;
    }

    public void OnClick()
    {
        onClickAction?.Invoke();
        myButton.image.color = Color.blue;
        myInventoryPanel.gameObject.SetActive(true);
        myInventoryPanel.isActive = true;
        myInventoryPanel.Refresh();
    }

    internal void Deselect()
    {
        myButton.image.color = Color.white;
    }
}
