﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
    [SerializeField] Image myImage;
    [SerializeField] TextMeshProUGUI itemName;
    [SerializeField] Button myButton;

    ItemBaseData myItemData;
    int globalIndex = -1;

    internal void SetUp(ItemBaseData itemData, int globalIndex)
    {
        this.globalIndex = globalIndex;
        if (itemData)
        {
            myImage.sprite = itemData.sprite;
            itemName.SetText(itemData.itemNameKey + (itemData.amount > 1 ? " x" + itemData.amount : ""));
            myItemData = itemData;
            myButton.onClick.AddListener(delegate ()
            {
                myButton.image.color = Color.green;
                InventoryManager.Instance.ItemButtonOnClickAction(myItemData);
            });
        }
        else
        {
            myImage.gameObject.SetActive(false);
            itemName.gameObject.SetActive(false);
            myButton.onClick.AddListener(delegate ()
            {
                myButton.image.color = Color.green;
                InventoryManager.Instance.ClickedEmptySpace(globalIndex);
            });
        }
    }
}
