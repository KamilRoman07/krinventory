﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleInventoryPanel : MonoBehaviour
{
    [SerializeField] GameObject itemButtonPrefab;
    [SerializeField] Transform panelGrid;

    List<ItemBaseData> itemsInInventory;
    int index;
    bool lastPage;

    public bool isActive = false;

    public void SetUp(int index, List<ItemBaseData> itemsToShow, bool lastPage)
    {
        this.index = index;
        itemsInInventory = itemsToShow;
        this.lastPage = lastPage;
    }

    public void Refresh()
    {
        int globalIndex = 0;
        if (isActive)
        {
            ClearItems();

            int playerMaxItems = GameManager.Instance.CurrentPlayerData.maksItemSlotsInInventory;
            int maxIndex = 28;
            if (lastPage)
            {
                if (playerMaxItems < 28)
                    maxIndex = playerMaxItems;
                else if (playerMaxItems > 28 && (playerMaxItems - index * 28) % 28 != 0)
                    maxIndex = playerMaxItems % 28;
                else
                    maxIndex = 28;
            }

            for (int i = 0; i < maxIndex; i++)
            {
                globalIndex = index * 28 + i;
                GameObject button = Instantiate(itemButtonPrefab);
                ItemButton itemButton;
                button.TryGetComponent(out itemButton);
                if (itemButton)
                    itemButton.SetUp(itemsInInventory.Count > index * 28 + i ? itemsInInventory[index * 28 + i] : null, globalIndex);
                button.transform.SetParent(panelGrid, false);
            }
        }
    }

    public void ClearItems()
    {
        foreach (Transform t in panelGrid)
            Destroy(t.gameObject);
    }
}
