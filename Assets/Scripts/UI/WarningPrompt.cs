﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WarningPrompt : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI warningText;
    [SerializeField] float flashMessageTime = 2f;

    public void FlashMessage(string message)
    {
        warningText.SetText(message);
        gameObject.SetActive(true);
        StopAllCoroutines();
        StartCoroutine(HideMessage(flashMessageTime));
    }

    public IEnumerator HideMessage(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }

    public void HideMessage()
    {
        gameObject.SetActive(false);
    }
}
