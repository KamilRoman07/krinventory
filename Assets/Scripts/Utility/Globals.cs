﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals
{
    //Layers
    public static string[] raycastIgnorableLayers = { "Ignore Raycast", "TransparentFX" };

    //Cinemachine
    public static int activeCinemachineCameraPriority = 999;
    public static int inactiveCinemachineCameraPriority = -1;
}
