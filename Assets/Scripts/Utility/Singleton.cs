﻿using System;
using UnityEngine;

public class SingeltonHelp
{
    public static Action DestroyAllSingeltonsOnGameReload;
}


public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T _instance;

    private static object _lock = new object();

	public static bool IsReady
	{
		get
		{
			lock (_lock)
			{
				return _instance != null;
			}
		}
	}

	public static T Instance
    {
        get
        {
            if (applicationIsQuitting)
            {
                Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                                 "' already destroyed on application quit." +
                                 " Won't create again - returning null.");
                return null;
            }

            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = (T)FindObjectOfType(typeof(T));

                    if (FindObjectsOfType(typeof(T)).Length > 1)
                    {
                        Debug.LogError("[Singleton] Something went really wrong " + typeof(T) +
									   " - there should never be more than 1 singleton!" +
                                       " Reopening the scene might fix it.");
                        return _instance;
                    }

                    if (_instance == null)
                    {
                        GameObject singleton = new GameObject();
                        _instance = singleton.AddComponent<T>();
                        singleton.name = "(singleton) " + typeof(T).ToString();
						
                        DontDestroyOnLoad(singleton);
                        SingeltonHelp.DestroyAllSingeltonsOnGameReload += () => DestroySingletonOnGameReload(singleton);
                    }
                }

                return _instance;
            }
        }
    }

    private static void DestroySingletonOnGameReload(GameObject gameObject)
    {
        SingeltonHelp.DestroyAllSingeltonsOnGameReload -= () => DestroySingletonOnGameReload(gameObject);
        if (gameObject)
        {
            Destroy(gameObject);
        }
    }

    private static bool applicationIsQuitting = false;
}
