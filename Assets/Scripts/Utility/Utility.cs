﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Utility
{
    public static bool ClosestPriorityHit(Ray ray, float distance, out RaycastHit result, int layerMask, string[] ignoreTags, bool onlyStatic, string[] objectPriorityTags)
    {
        result = default;
        result.distance = float.MaxValue;

        var results = Physics.RaycastAll(ray, distance, layerMask);

        if (results.Length == 0)
            return false;

        RaycastHit resultWithNoTags = results[0];

        foreach (var type in objectPriorityTags)
        {
            int i = 0;

            for (; i < results.Length; ++i)
            {
                var r = results[i];

                if ((onlyStatic && !r.collider.gameObject.isStatic) || HasTag(r.collider, ignoreTags))
                    continue;

                if (r.collider.CompareTag(type))
                    result = r;
                else
                    resultWithNoTags = r;

                break;
            }

            for (; i < results.Length; ++i)
            {
                var r = results[i];
                if ((onlyStatic && !r.collider.gameObject.isStatic))
                    continue;

                if (resultWithNoTags.distance > r.distance && !HasTag(r.collider, ignoreTags))
                    resultWithNoTags = r;

                if ((r.collider.CompareTag(type)) && result.distance > r.distance && !HasTag(r.collider, ignoreTags))
                    result = r;
            }

            if (result.collider)
                return true;
        }

        result = resultWithNoTags;

        return result.collider != null;
    }

    private static bool HasTag(Collider col, string[] tags)
    {
        foreach (var tag in tags)
            if (col.CompareTag(tag))
                return true;
        return false;
    }

    public static T FindObjectOnScenes<T>(bool includeHidden) where T : Behaviour
    {
        for (int s = 0; s < SceneManager.sceneCount; ++s)
        {
            var scene = SceneManager.GetSceneAt(s);

            var obj = FindObjectOnScene<T>(scene, includeHidden);
            if (obj)
                return obj;
        }
        return null;
    }

    public static T FindObjectOnScene<T>(Scene scene, bool includeHidden) where T : Behaviour
    {
        var ro = scene.GetRootGameObjects();
        for (int r = 0; r < ro.Length; ++r)
        {
            var obj = ro[r].GetComponentInChildren<T>(includeHidden);
            if (obj)
                return obj;
        }
        return null;
    }
}
